var express = require('express');
var router = express.Router();
var tablaSchools = require('../models/schools');

/* GET users listing. */
router.get('/', function(req, res, next) {
});


router.post('/schools', function(req, res, next) {  

   var query = {

	"Nombre"			    :req.body.Nombre,
	"Clave"				    :req.body.Clave,
	"NivelEducativo"	:req.body.NivelEducativo,
	"Estado"			    :req.body.Estado,
	"Direccion"			  :req.body.Direccion,
	"CP"				      :req.body.CP,
	"Turnos"			    :req.body.Turnos,
	"Telefono"			  :req.body.Telefono
   };

  console.log(query);
   tablaSchools.build(query).save().then(function(){
  var createjson = {"message": "Cool", "created": true};
   res.json(createjson);
   	}).catch(function(err){
   		console.log(err);
   	});
 

});

module.exports = router;
