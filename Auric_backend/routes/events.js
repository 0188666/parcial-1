var express = require('express');
var router = express.Router();
var eventTable = require("../models/event");


router.get('/event/:id', function(req, res, next) {
	var events;
	if(req.params.id){
		eventTable.findOne({
			where: {
		        id : id
	      	}
      	}).then(function(events){            
            res.json(events);
        });		
	}
	else{
		eventTable.findAll().then(function(events) {            
            res.json( events );
        });				
	}	

});

router.post('/event', function(req, res, next) {  
	var event = {			
		
	}
	for (var column in req.body) {
		event[column] = req.body[column];
    }
	eventTable.build(event).save().catch(function (err) {
        console.log(err);
    });
});


router.put('/event/:id', function(req, res, next) {
	var id = req.params.id;
	var event = { }

	for (var column in req.body) {
		event[column] = req.body[column];
    }

	eventTable.update(event, {
      where: {
        id : id
      }
    }).catch(function (err) {
        console.log("Caught error on job: " + err);
    });
});


router.delete('/event/:id', function(req, res, next) {
	var id = req.params.id;
	
	eventTable.destroy({
	    where: {
	        id : id
	    }
	});
	
});


module.exports = router;