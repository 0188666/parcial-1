var express = require('express');
var router = express.Router();
var eventTable = require("../models/event");
var photographerTable = require("../models/photographer");
var sha256=require('sha256');
var signinTable = require("../models/signin");


router.post('/signin', function(req, res, next) {  
  console.log("hi");
  //emailX
  //name
  console.log( sha256(req.body.password));
  var newUserData = {
      "email":req.body.email, 
      "name":req.body.name, 
      "username":req.body.username, 
      "password":sha256(req.body.password),
      "address":req.body.address,
  };
  console.log("Creando usuario...");
  console.log(newUserData);
var createJson={};
    signinTable.build(newUserData).save().then(function(){
        //SUCCESS!! :)
   createJson = {"message":"Se ha creado el usuario con exito!", "created":true };          
       
       // res.json(createJson);
    }).catch(function (err) {
        //ERROR!! :(
        console.log(err);
		createJson = {"message":"Ha ocurrido un error :(", "created":false };  				 
        res.json(createJson);
    });

    res.json(createJson);
 

});



router.post('/login', function(req, res, next) {
  var responselogin = {};
  console.log(req.body);

  var query = {"email":req.body.email, "password":sha256(req.body.password) };

  signinTable.findOne( { where: query } ).then(function(user)
	{
		if(user){
			responselogin = {"message":"Login exitoso", "login":true };  				 
		}else{
			responselogin = {"message":"Login fallido", "login":false };  	  	
		}
  		res.json(responselogin);
	});


});


module.exports = router;