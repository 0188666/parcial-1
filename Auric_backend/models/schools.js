var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('AuricDB', 'root', 'Tokyo.moon1',{
  host: mysql_host,
  dialect: 'mysql'
});


var schools = sequelize.define('schools', {
	"Nombre"				: { type: Sequelize.STRING },
	"Clave"				: { type: Sequelize.STRING, primaryKey: true },
	"NivelEducativo"				: { type: Sequelize.STRING },
	"Estado"				: { type: Sequelize.STRING },
	"Direccion"				: { type: Sequelize.STRING },
	"CP"				: { type: Sequelize.INTEGER },
	"Turnos"				: { type: Sequelize.STRING },
	"Telefono"				: { type: Sequelize.INTEGER }

});

schools.sync();
module.exports = schools;